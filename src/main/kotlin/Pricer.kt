object Pricer
{
    private const val NO_DISCOUNT = 0.0
    private const val THREE_PERCENT_DISCOUNT_THRESHOLD = 1000
    private const val FIVE_PERCENT_DISCOUNT_THRESHOLD = 5000
    private const val THREE_PERCENT = 0.03
    private const val FIVE_PERCENT = 0.05

    fun calculate(nbArticle: Int, unitPrice: Double, tax: Int): String
    {
        val totalPrice = nbArticle * priceWithTax(unitPrice, tax)

        val discount = when
        {
            totalPrice.exceedsDiscountThreshold() -> getDiscount(totalPrice)
            else                                  -> NO_DISCOUNT
        }

        return "${PriceFormatter.format(totalPrice - discount)} €"
    }

    private fun priceWithTax(price: Double, tax: Int) = price + (price * tax / 100.0)

    private fun Double.exceedsDiscountThreshold() = this > THREE_PERCENT_DISCOUNT_THRESHOLD

    private fun getDiscount(totalPrice: Double): Double
    {
        val discountPercentage = when
        {
            totalPrice > FIVE_PERCENT_DISCOUNT_THRESHOLD -> FIVE_PERCENT
            else                                         -> THREE_PERCENT
        }

        return totalPrice * discountPercentage
    }
}
