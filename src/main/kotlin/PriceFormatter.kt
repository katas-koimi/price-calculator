import java.util.*

object PriceFormatter
{
    fun format(price: Double) = String.format(Locale.US, "%.2f", price)
}
