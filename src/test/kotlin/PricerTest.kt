import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PricerTest
{
    @Test
    fun `one article without tax`()
    {
        assertEquals("1.00 €", Pricer.calculate(1, 1.0, 0))
    }

    @Test
    fun `one article without tax (other price)`()
    {
        assertEquals("67.00 €", Pricer.calculate(1, 67.0, 0))
    }

    @Test
    fun `multiple articles without tax`()
    {
        assertEquals("52.00 €", Pricer.calculate(4, 13.0, 0))
    }

    @Test
    fun `article with tax`()
    {
        assertEquals("1.10 €", Pricer.calculate(1, 1.0, 10))
    }

    @Test
    fun `article with tax (other case)`()
    {
        assertEquals("3.81 €", Pricer.calculate(3, 1.21, 5))
    }

    @Test
    fun `when total price exceeds 1000 euros then apply a discount of 3 percent`()
    {
        assertEquals("1840.58 €", Pricer.calculate(5, 345.0, 10))
    }

    @Test
    fun `when total price exceeds 5000 euros then apply a discount of 5 percent`()
    {
        assertEquals("6787.28 €", Pricer.calculate(5, 1299.0, 10))
    }
}